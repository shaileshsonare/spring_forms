package controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 * Ref Link: http://docs.spring.io/spring-security/site/docs/current/apidocs/org/springframework/security/crypto/bcrypt/BCrypt.html
 * Ref Link: https://gist.github.com/craSH/5217757
 * Ref Link: http://www.java2s.com/Code/Jar/s/Downloadspringsecuritycrypto310RELEASEjar.htm
 * Cookie Reference: http://viralpatel.net/blogs/spring-mvc-cookie-example/
 */
@Controller
@RequestMapping("/employee")
public class Employee {
    
    String passwd;
    
    @RequestMapping("/signupform")
    public ModelAndView signUpForm(HttpSession session) {
        //session.setAttribute("mysessionvar", "S4671314");
        return new ModelAndView("signup");
    }
    
    @RequestMapping("/signupaction")
    public void signUp(@RequestParam("email") String email, @RequestParam("password") String password) {
        System.out.println("Email: " + email + " Password " + password);
        System.out.println("Date : " + new Date());
        // once you get your data here you can send this data to DAO object and save it in database
    }
    
    @RequestMapping("/loginform")
    public ModelAndView logInForm() {
        return new ModelAndView("login");
    }
    
    @RequestMapping("/createcookies")
    public ModelAndView createCookie(@CookieValue(value = "myCookieName", defaultValue = "defaultCookieValue") String cookieValue, HttpServletResponse response, HttpServletRequest request) {
        /* Creating new cookie */
        Cookie foo = new Cookie("myCookieName", "The cookie's value"); // myCookieName = The cookie's value
        foo.setMaxAge(60); // time in seconds in this case 60 seconds
        response.addCookie(foo); // add cookie ofject in response
        System.out.println(Arrays.toString(request.getCookies()));;
        /* Creating new cookie */

        return new ModelAndView("createcookie");
    }
    
    @RequestMapping("/readcookie")
    public ModelAndView readCookie(HttpServletRequest request) {
    
        /* Reading cookie values */
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                System.out.println("" + cookie.getName() + "=" + cookie.getValue());

                if (cookie.getName().equals("cookieName")) {
                    //do something
                    //value can be retrieved using #cookie.getValue()
                }
            }
        }
        /* Reading cookie values */
        return new ModelAndView("readcookies");
    }
    
    @RequestMapping("/loginaction")
    public ModelAndView loginAction(@RequestParam("email") String email, @RequestParam("password") String password) {
        System.out.println("Email: " + email + " Password " + password);
        // once you get your data here you can send this data to DAO 
        // and check whether record is exist or not
        // e.g. SQL = "From Employee where email = email and password = password
       if(email.equals("shaileshsonare@gmail.com") && password.equals("123456")) {
            return new ModelAndView("welcomepage");
       } else {
            return new ModelAndView("accessdenied");
       }
    }
    
    @RequestMapping (value = "/register", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView renderPromotePage (HttpServletRequest request) {
        Map<String, String[]> parameters = request.getParameterMap();

        for(String key : parameters.keySet()) {
            System.out.println(key);
            String[] vals = parameters.get(key);
            for(String val : vals)
                System.out.println(" -> " + val);
        }

        ModelAndView mv = new ModelAndView();
        mv.setViewName("test");
        return mv;
    }
    
    @RequestMapping (value = "/ajaxcall", method = {RequestMethod.GET})
    public @ResponseBody String callAjax(HttpSession session, @RequestParam("email_id") String email_id) {
        // create dao object send email_id to it and get response from any table
        //session.setAttribute("email_id", email_id);
        //session.removeAttribute("email_id");
        Enumeration e = session.getAttributeNames();
        while(e.hasMoreElements()){
            System.out.println("" + e.nextElement());
        }
        
        return "I am ajax Response " + email_id + " " + session.getAttribute("email_id");
    }
    
    @RequestMapping("/bcryptpasswd")
    public ModelAndView checkPassword(@RequestParam(value = "passwd", defaultValue = "Test") String password) {

        String pw_hash = BCrypt.hashpw(password, BCrypt.gensalt()); 
        
        this.passwd = pw_hash;
        
        ModelAndView mv = new ModelAndView("bcryptpassword");
        mv.addObject("input_password", password);
        mv.addObject("hashed_password", pw_hash);
                
        mv.addObject("msg", "Sample Message");
        
        return mv;
    }
    
    @RequestMapping("/chkbcryptpasswd")
    public ModelAndView checkHashedPassword(@RequestParam(value = "passwd", defaultValue = "Test") String password) {

        ModelAndView mv = new ModelAndView("bcryptpassword");
        mv.addObject("input_password", password);
        mv.addObject("hashed_password", this.passwd);
        
        if (BCrypt.checkpw(password, this.passwd))
           mv.addObject("msg", "It matches");
        else
           mv.addObject("msg", "It does not match");
        
        return mv;
    }
}
