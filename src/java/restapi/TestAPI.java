/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restapi;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 * 
 * To run this api hit 
 * http://localhost:8080/SpringForms/api/test
 * and 
 * http://localhost:8080/SpringForms/api/test/test2
 * on browser
 */
@Path("test")
public class TestAPI {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TestAPI
     */
    public TestAPI() {
    }

    /**
     * Retrieves representation of an instance of restapi.TestAPI
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getHtml() {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        return "<html><body><h1>Rest API Working Fine</h1><body></html>";
    }
    
    @GET
    @Path("test2")
    @Produces(MediaType.TEXT_HTML)
    public String getHtml2() {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        return "<html><body><h1>Rest API Working Fine with Multiple methods</h1><body></html>";
    }

    /**
     * PUT method for updating or creating an instance of TestAPI
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_HTML)
    public void putHtml(String content) {
    }
}
