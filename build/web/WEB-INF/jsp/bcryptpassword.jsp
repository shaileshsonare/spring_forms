<%-- 
    Document   : bcryptpassword
    Created on : 8 Dec, 2016, 12:39:36 PM
    Author     : Shailesh Sonare 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>${msg}</h1>
        <h1>${input_password}</h1>
        <h1>${hashed_password}</h1>
    </body>
</html>
