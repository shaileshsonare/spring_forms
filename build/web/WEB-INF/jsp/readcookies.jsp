<%-- 
    Document   : readcookies
    Created on : 21 Dec, 2016, 6:24:00 PM
    Author     : Shailesh Sonare 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h3>Below is cookie value</h3>
        ${cookie.myCookieName.value}
    </body>
</html>
