<%-- 
    Document   : createcookie
    Created on : 21 Dec, 2016, 6:23:37 PM
    Author     : Shailesh Sonare 
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cookies Created</h1>
        <a href="<c:url value="/employee/readcookie.htm"/>">Show Cookies</a>
    </body>
</html>
