<%-- 
    Document   : signup
    Created on : 22 Nov, 2016, 10:59:04 AM
    Author     : Shailesh Sonare 
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>SignUp</h1>
        <form method="post" action="<c:url value="/employee/signupaction.htm" />">
            <table>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email" id="email"/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="password"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Sign Up" name="signup"/></td>
                    <td><input type="reset" vlaue="Clear" name="reset"/></td>
                </tr>
            </table>
            <c:if test='${mysessionvar == "S4671314"}'>
                Hello WOrld
            </c:if>
                
                <c:choose>
                    <c:when test='${mysessionvar == "S4671314"}'>
                        Hello World
                    </c:when>
                    <c:otherwise>
                        Bye World
                    </c:otherwise>
                </c:choose>
        </form>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        $('#email').on("focusout", function() {
           
           var input_email_id = $('#email').val();
            
           $.ajax({
               
               url: "<c:url value="/employee/ajaxcall.htm"/>",
               method: "GET",
               data : {email_id : input_email_id},
               success: function(data) {
                    console.log(data);
               }
               
           }).done().fail().always(); 
        });
    </script>
</html>
